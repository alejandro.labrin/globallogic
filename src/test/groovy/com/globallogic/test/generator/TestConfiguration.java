package com.globallogic.test.generator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.WebApplicationContext;

@Configuration
public class TestConfiguration extends SpringBootServletInitializer {

  @Autowired
  private WebApplicationContext context;
  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
      return application.sources(TestConfiguration.class);
  }

  public static void main(String[] args) {
      SpringApplication.run(TestConfiguration.class, args);
  }
}
