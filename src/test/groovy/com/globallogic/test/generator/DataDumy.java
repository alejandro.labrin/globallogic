package com.globallogic.test.generator;

import com.globallogic.test.dto.PhoneDTO;
import com.globallogic.test.dto.UserRequestDTO;

import java.util.ArrayList;
import java.util.Collection;


public class DataDumy {

  public UserRequestDTO getRegisterAllData() {
    UserRequestDTO userDTO = new UserRequestDTO();
    userDTO.setName("Juan Perez");
    userDTO.setEmail("juan.perez_cl@gmail.com");
    userDTO.setPassword("Herna12");
    userDTO.setPhones(getPhone());
    return userDTO;
}

  public Collection<PhoneDTO> getPhone() {
    Collection<PhoneDTO> phoneDtos = new ArrayList<>();
    PhoneDTO phoneDto = new PhoneDTO();
    phoneDto.setNumber("1234567890");
    phoneDto.setCitycode("1");
    phoneDto.setCountrycode("56");
    phoneDtos.add(phoneDto);
    return phoneDtos;
}
  
}
