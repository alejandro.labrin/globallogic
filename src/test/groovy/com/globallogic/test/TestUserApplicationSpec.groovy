package com.globallogic.test

import com.globallogic.test.dto.UserRequestDTO
import com.globallogic.test.generator.DataDumy
import com.globallogic.test.utils.Utils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.web.context.WebApplicationContext
import spock.lang.Specification
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TestUserApplicationSpec extends Specification{

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private MockMvc mvc;

    def mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

    def "When send correct user data then the response has status 201 (User Create)"() {
        expect: "Status is 201 and user is created"
        DataDumy generador = new DataDumy()
        UserRequestDTO userdto = generador.getRegisterAllData()
        this.mvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(Utils.asJsonString(userdto)))
                .andExpect(status().isCreated());
    }

    def "When send correct user data to non-exist endpoint the response has status 403 forbidden"() {
        expect: "Status is 403 forbidden"
        DataDumy generador = new DataDumy();
        this.mvc.perform(post("/register2").contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON).content(Utils.asJsonString(generador)))
                .andExpect(status().isForbidden());
    }

    def "When send duplicate email user data then the response status precondition required and show message error"() {
        expect: "Show message: El correo ya se encuentra registrado."
        DataDumy generador = new DataDumy();
        UserRequestDTO userdto = generador.getRegisterAllData();
        userdto.setEmail("juan.perez@gmail.com");
        this.mvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.asJsonString(userdto)))
                .andDo(print())
                .andExpect(status().isPreconditionRequired())
                .andExpect(jsonPath(".mensaje").value("El correo ya se encuentra registrado."));
    }

    def "When send null email then the response status precondition required and show message error"() {
        expect: "Show message: Debe informar un email válido."
        DataDumy generador = new DataDumy();
        UserRequestDTO userdto = generador.getRegisterAllData();
        userdto.setEmail(null);
        this.mvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.asJsonString(userdto)))
                .andDo(print())
                .andExpect(status().isPreconditionRequired())
                .andExpect(jsonPath(".mensaje").value("Debe informar un email válido."));
    }

    def "When send a malformed email address then the response status precondition required and show message error"() {
        expect: "Show message: Debe informar un email válido."
        DataDumy generador = new DataDumy();
        UserRequestDTO userdto = generador.getRegisterAllData();
        userdto.setEmail("aaaaa.org");
        this.mvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.asJsonString(userdto)))
                .andDo(print())
                .andExpect(status().isPreconditionRequired())
                .andExpect(jsonPath(".mensaje").value("Debe informar un email válido."));
    }

    def "When send name with null value then the response status precondition required and show message error"() {
        expect: "Show message: Campo name debe ser informado."
        DataDumy generador = new DataDumy();
        UserRequestDTO userdto = generador.getRegisterAllData();
        userdto.setName(null);
        this.mvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.asJsonString(userdto)))
                .andDo(print())
                .andExpect(status().isPreconditionRequired())
                .andExpect(jsonPath(".mensaje").value("Campo name debe ser informado."));
    }

    def "When send a malformed password then the response status precondition required and show message error"() {
        expect: "Show message: Debe informar un password válido. [debe contener una mayúscula, letras minúsculas, y dos números]"
        DataDumy generador = new DataDumy();
        UserRequestDTO userdto = generador.getRegisterAllData();
        userdto.setPassword("miema.org");
        this.mvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.asJsonString(userdto)))
                .andDo(print())
                .andExpect(status().isPreconditionRequired())
                .andExpect(jsonPath(".mensaje").value("Debe informar un password válido. [debe contener una mayúscula, letras minúsculas, y dos números]"));
    }
}
