package com.globallogic.test.controller;

import com.globallogic.test.dto.ResponseMessageDTO;
import com.globallogic.test.helper.UserHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.globallogic.test.dto.UserRequestDTO;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class UserController {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

  @Autowired
  private UserHelper userHelper;

  @PostMapping(path = "/register", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> register(@RequestBody UserRequestDTO userDTO) {
    LOGGER.info("register():Inicio");
    try {
      return new ResponseEntity<>(userHelper.createUser(userDTO), HttpStatus.CREATED);
    } catch (Exception e){
      ResponseMessageDTO responseMessage = new ResponseMessageDTO();
      responseMessage.setMessage(e.getMessage());
      return new ResponseEntity<>(responseMessage, HttpStatus.PRECONDITION_REQUIRED);
    }


}




}
