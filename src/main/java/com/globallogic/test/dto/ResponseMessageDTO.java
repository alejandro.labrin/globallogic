package com.globallogic.test.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseMessageDTO {
  @JsonProperty("mensaje")
  private String message;

  public ResponseMessageDTO(){

  }

  public ResponseMessageDTO(String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
