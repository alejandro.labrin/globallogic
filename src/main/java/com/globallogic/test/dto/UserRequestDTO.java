package com.globallogic.test.dto;

import java.util.Collection;


public class UserRequestDTO {
  
  private String name;
  private String email;
  private String password;
  private String token;
  private Collection<PhoneDTO> phones;

  public UserRequestDTO(){

  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public Collection<PhoneDTO> getPhones() {
    return phones;
  }

  public void setPhones(Collection<PhoneDTO> phones) {
    this.phones = phones;
  }
}
