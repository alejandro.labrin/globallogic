package com.globallogic.test.constants;

public class Message {
  
  public static final String MESSAGE_NULL_OR_EMPTY_NAME = "Campo name debe ser informado.";
  public static final String MESSAGE_NOT_VALID_EMAIL = "Debe informar un email válido.";
  public static final String MESSAGE_NOT_VALID_PASSWORD = "Debe informar un password válido. [debe contener una mayúscula, letras minúsculas, y dos números]";
  public static final String MESSAGE_USER_EXISTS = "El correo ya se encuentra registrado.";
  public static final String MESSAGE_NULL_OR_EMPTY_PHONES = "Debe informar al menos un télefono.";
  public static final String MESSAGE_NULL_OR_EMPTY_NUMBER = "Campo number debe ser informado.";
  public static final String MESSAGE_NULL_OR_EMPTY_CITY_CODE = "Campo citycode debe ser informado.";
  public static final String MESSAGE_NULL_OR_EMPTY_COUNTRY_CODE = "Campo countrycode debe ser informado.";
}
