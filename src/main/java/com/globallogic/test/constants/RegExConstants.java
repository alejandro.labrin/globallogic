package com.globallogic.test.constants;

public class RegExConstants {

    public static final String EMAIL_FORMAT = "^([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$";
    public static final String PASS_FORMAT = "^(?=(?:.*\\d){2})(?=(?:.*[A-Z]){1})(?=(?:.*[a-z]){1})\\S{4,}$";
}
