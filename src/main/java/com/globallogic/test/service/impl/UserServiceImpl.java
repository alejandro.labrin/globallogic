package com.globallogic.test.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.globallogic.test.constants.Message;
import com.globallogic.test.dto.UserRequestDTO;
import com.globallogic.test.model.UsersModel;
import com.globallogic.test.repository.UserRepository;
import com.globallogic.test.service.UserService;
import com.globallogic.test.util.DtoUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@AllArgsConstructor
@Transactional
public class UserServiceImpl implements UserService {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
  
  @Autowired
  private UserRepository userRepository;
  
  @Transactional
  public UsersModel save(UserRequestDTO userDTO){
    LOGGER.info("save():Inicio");
    LOGGER.debug("userDTO: ",  userDTO.toString());
    UsersModel users = DtoUtil.convertUserDTOtoModel(userDTO);
    userRepository.save(users);
    LOGGER.info("save():Fin");
    return users;
  }
  
  public void usuarioExiste(String email) throws  Exception{
    LOGGER.info("usuarioExiste():Inicio");
    LOGGER.debug("email: ",  email);
    Optional<UsersModel> users = userRepository.findByEmail(email) ;
    if(users.isPresent()) {
      LOGGER.error("Usuario existe: [".concat(email).concat("]"));
      throw new Exception(Message.MESSAGE_USER_EXISTS);
    }
    LOGGER.info("usuarioExiste():Fin");
  }

}
