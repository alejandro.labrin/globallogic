package com.globallogic.test.service;

import com.globallogic.test.dto.UserRequestDTO;
import com.globallogic.test.model.UsersModel;

public interface UserService {

  UsersModel save(UserRequestDTO userDTO) throws Exception ;
  void usuarioExiste(String email) throws  Exception;
}
