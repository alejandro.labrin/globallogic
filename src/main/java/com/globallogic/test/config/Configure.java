package com.globallogic.test.config;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;

public class Configure {
  protected void configure(HttpSecurity http) throws Exception {
    http.httpBasic(); http.csrf().disable();
    http.authorizeRequests().antMatchers("/").permitAll().and()
    .authorizeRequests().antMatchers("/register/**").permitAll(); 
    http.headers().frameOptions().disable();}
}
