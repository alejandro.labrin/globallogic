package com.globallogic.test.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.globallogic.test.model.UsersModel;


@Repository
public interface UserRepository extends JpaRepository<UsersModel, String> {

  Optional<UsersModel> findByEmail(String email);
}
