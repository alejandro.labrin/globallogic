package com.globallogic.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.globallogic.test.model.PhonesModel;

@Repository
public interface PhoneRepository extends JpaRepository<PhonesModel, String> {

}
