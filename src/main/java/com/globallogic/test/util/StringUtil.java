package com.globallogic.test.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {

    public static boolean isEmpty(String value){
        boolean isEmpty = false;
        if(value == null || value.isEmpty()){
            isEmpty = true;
        }
        return isEmpty;
    }

    public static boolean isValidFormat(String value, String format){
        if(value != null){
            Pattern pattern = Pattern.compile(format);
            Matcher matcher = pattern.matcher(value);
            return matcher.find();
        }
        return false;
    }
}
