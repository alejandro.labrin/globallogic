package com.globallogic.test.util;

import java.util.ArrayList;
import java.util.List;
import com.globallogic.test.dto.PhoneDTO;
import com.globallogic.test.dto.UserRequestDTO;
import com.globallogic.test.dto.UserResponseDTO;
import com.globallogic.test.model.PhonesModel;
import com.globallogic.test.model.UsersModel;

public class DtoUtil {


  /**
   * Metodo para convertir dto user a model
   * @param userDTO
   * @return
   */
  public static UsersModel convertUserDTOtoModel(UserRequestDTO userDTO) {
    UsersModel users = new UsersModel();
    List<PhonesModel>  phones= new ArrayList <>();
    users.setName(userDTO.getName());
    users.setEmail(userDTO.getEmail());
    users.setPassword(userDTO.getPassword());
    users.setToken(userDTO.getToken());
    userDTO.getPhones().forEach(phonesDTO ->phones.add(convertPhoneDTOtoModel(phonesDTO,users))); //expresion lambda java 8
    return users;
  }

  /**
   * Metodo aux  para convertir phones en model phone
   * @param phoneDTO
   * @param users
   * @return
   */
  public static PhonesModel convertPhoneDTOtoModel(PhoneDTO phoneDTO, UsersModel users) {
    PhonesModel phones = new PhonesModel();
    phones.setNumber(phoneDTO.getNumber());
    phones.setCountryCode(phoneDTO.getCountrycode());
    phones.setCityCode(phoneDTO.getCitycode());
    phones.setUsers(users);
    return phones;
  }

  /**
   * Metodo aux para convertir entidad user a objeto DTO
   * @param entity
   * @return
   */
  public static UserResponseDTO convertUserModelToDTO(UsersModel entity){
    UserResponseDTO userResponseDTO = new UserResponseDTO();
    userResponseDTO.setId(entity.getId());
    userResponseDTO.setName(entity.getName());
    userResponseDTO.setCreated(entity.getCreated());
    userResponseDTO.setModified(entity.getModified());
    userResponseDTO.setLastLogin(entity.getLastLogin());
    userResponseDTO.setToken(entity.getToken());
    userResponseDTO.setActive(entity.getIsActive());
    return userResponseDTO;
  }
}
