package com.globallogic.test.helper;

import com.globallogic.test.constants.Message;
import com.globallogic.test.constants.RegExConstants;
import com.globallogic.test.dto.PhoneDTO;
import com.globallogic.test.dto.UserRequestDTO;
import com.globallogic.test.dto.UserResponseDTO;
import com.globallogic.test.service.UserService;
import com.globallogic.test.util.DtoUtil;
import com.globallogic.test.util.StringUtil;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service("userHelper")
public class UserHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserHelper.class);

    @Autowired
    UserService userService;

    /**
     * Metodo para procesamiento de creacion de usuario.
     * @param userDTO
     * @return
     * @throws Exception
     */
    public UserResponseDTO createUser(UserRequestDTO userDTO) throws Exception {
        LOGGER.info("createUser():Inicio");
        //Valida parametros de entrada
        validateRequest(userDTO);

        //Valida si usuario existe mediante el correo
        userService.usuarioExiste(userDTO.getEmail());

        //Generacion de token
        String token = getJWTToken(userDTO.getName());
        userDTO.setToken(token);

        //Creacion de usuario nuevo
        UserResponseDTO response = DtoUtil.convertUserModelToDTO(userService.save(userDTO));
        LOGGER.info("createUser():Fin");
        return response;
    }

    private String getJWTToken(String username) {
        String secretKey = "global";
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("ROLE_USER");

        String token = Jwts
                .builder()
                .setId("globalJWT")
                .setSubject(username)
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 600000))
                .signWith(SignatureAlgorithm.HS512,
                        secretKey.getBytes()).compact();

        return "Bearer " + token;
    }

    public void validateRequest(UserRequestDTO userDTO) throws Exception {
        if(StringUtil.isEmpty(userDTO.getName())){
            throw new Exception(Message.MESSAGE_NULL_OR_EMPTY_NAME);
        } else if(StringUtil.isEmpty(userDTO.getEmail()) || !StringUtil.isValidFormat(userDTO.getEmail(), RegExConstants.EMAIL_FORMAT)){
            throw new Exception(Message.MESSAGE_NOT_VALID_EMAIL);
        } else if(StringUtil.isEmpty(userDTO.getPassword()) || !StringUtil.isValidFormat(userDTO.getPassword(), RegExConstants.PASS_FORMAT)){
            throw new Exception(Message.MESSAGE_NOT_VALID_PASSWORD);
        } else {
            if(userDTO.getPhones() == null || userDTO.getPhones().isEmpty()){
                throw new Exception(Message.MESSAGE_NULL_OR_EMPTY_PHONES);
            } else {
                for (PhoneDTO phoneDTO: userDTO.getPhones()) {
                    if(StringUtil.isEmpty(phoneDTO.getNumber())){
                        throw new Exception(Message.MESSAGE_NULL_OR_EMPTY_NUMBER);
                    } else if(StringUtil.isEmpty(phoneDTO.getCitycode())){
                        throw new Exception(Message.MESSAGE_NULL_OR_EMPTY_CITY_CODE);
                    } else if(StringUtil.isEmpty(phoneDTO.getCountrycode())){
                        throw new Exception(Message.MESSAGE_NULL_OR_EMPTY_COUNTRY_CODE);
                    }
                }
            }
        }
    }


}
